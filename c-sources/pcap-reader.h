/*
 * pcap-reader.h - reads DBus messages from a pcap stream
 * Copyright © 2011–2012  Collabora Ltd.
 * Copyright © 2018–2020 Will Thompson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#pragma once

#include <glib/gstdio.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <pcap/pcap.h>

#define BUSTLE_TYPE_PCAP_READER bustle_pcap_reader_get_type ()
G_DECLARE_FINAL_TYPE (BustlePcapReader, bustle_pcap_reader, BUSTLE, PCAP_READER, GObject)

BustlePcapReader *bustle_pcap_reader_open     (const gchar       *filename,
                                               GError           **error);
BustlePcapReader *bustle_pcap_reader_fopen    (FILE              *filep,
                                               GError           **error);
gboolean          bustle_pcap_reader_read_one (BustlePcapReader  *self,
                                               glong             *sec,
                                               glong             *usec,
                                               const guchar     **blob,
                                               guint             *length,
                                               GDBusMessage     **message,
                                               GError           **error);
void              bustle_pcap_reader_close    (BustlePcapReader  *self);

